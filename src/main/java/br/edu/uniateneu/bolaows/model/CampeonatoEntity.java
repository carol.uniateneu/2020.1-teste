package br.edu.uniateneu.bolaows.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_competicao")
public class CampeonatoEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cd_competicao")
	private Long id;
	@Column(name = "nm_competicao")
	private String nome;
	@Column(name = "nr_ano")
	private int ano;
	@Column(name = "tp_competicao", nullable = false)
	@Enumerated(EnumType.STRING)
	private EnumTipoCompeticao tipo;

	public CampeonatoEntity(String nome, int ano, EnumTipoCompeticao tipo) {
		this.nome = nome;
		this.ano = ano;
		this.tipo = tipo;
	}

	public CampeonatoEntity() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public EnumTipoCompeticao getTipo() {
		return tipo;
	}

	public void setTipo(EnumTipoCompeticao tipo) {
		this.tipo = tipo;
	}

}
